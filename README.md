# final_project

## Name
final_project for my AOS 573 class

## Description
This is for my final class project (AOS 573), where I will use the skills I have learned in this class. I will develop your own python
code for analyzing an Earth science dataset, in this case "Clear Air Turbulence (CAT)" to answer a research question.

## Repo contents
1. Contains OMEGA from MERRA-2, Precipitation from IMERG and also data from the wyoming dataset of soundings.
2. "final_project" contains the code to make the analyze.
3. For the purpose of this project we made intermediate changes to the size of the data obtained from https://disc.gsfc.nasa.gov/datasets the code can be found on folder: "reduce_data" 
5. Images to understand the analysis inside the code.
6. A .yml file with the environment that I used for this project

